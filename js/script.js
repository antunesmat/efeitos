$(document).ready(function() {

    //Reference used to create the back-to-top button: https://codepen.io/yying6/pen/yGhHu

    // Show or hide the sticky footer button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
    
    // Animate the scroll to top
    $('.go-top').click(function(event) {
        event.preventDefault();
        
        $('html, body').animate({scrollTop: 0}, 300);
    })

    // Animate the scroll to top
    $(".arrow").click(function() {
        $('html, body').animate({
          scrollTop: $(".about").offset().top
        }, 300);
    });

    
    $('.carousel').slick();

});

function allowDrop(ev) {
    ev.preventDefault();
  }
  
  function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
  }